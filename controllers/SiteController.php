<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\News;
use dosamigos\transliterator\TransliteratorHelper;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [

        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {


        return $this->render('index');
    }


    public function actionCreate_news()
    {
        $model = new News();
        if ($model->load(Yii::$app->request->post())) {
            if (empty($model->translit_title)) $model->translit_title = TransliteratorHelper::process($model->title, '', 'en');

            if ($user = $model->create_news()) {
                return $this->goBack();
            }
        }
        return $this->render('add_news', [
            'model' => $model,
        ]);
    }

    public function actionViews()
    {
        $news = Yii::$app->request->get();

        if (!empty($news)) {
            $model = News::find()
                ->where(['translit_title' => $news['views']])
                ->orderBy('id')
                ->one();
            return $this->render('news', ['news' => $model]);
        }
    }
    public function actionDelete()
    {
        $news = Yii::$app->request->post();

        if (!empty($news)) {
            $news = News::findOne($news['id']); //так будеб быстрее , чем deleteALL
            $news->delete();
            return $this->goBack();
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
