<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $translit_title
 * @property string $short_content
 * @property string $content
 * @property string $date
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['short_content', 'content', 'translit_title'], 'string'],
            [['date'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'translit_title' => 'Translit Title',
            'short_content' => 'Short Content',
            'content' => 'Content',
            'date' => 'Date',
        ];
    }

    public function create_news()
    {
        if (!$this->validate()) {
            return null;
        }

        $news = new News();
        $news->title = $this->title;
        $news->translit_title = $this->translit_title;
        $news->content = $this->content;
        $news->short_content = $this->short_content;
        $news->date = $this->date;
        return $news->save() ? $news : null;
    }
}
