<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

?>
<div class="news-item">

    <h2><a href="<?= Url::to(['news/' . $news->translit_title]) ?>"><?= Html::encode($model->title) ?></h2>
    <?= HtmlPurifier::process($news->short_content) ?>
    <?= HtmlPurifier::process($news->content) ?>
    <?= HtmlPurifier::process($news->date) ?>
</div>