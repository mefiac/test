<?php
/* @var $this yii\web\View */
use app\models\News;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

$dataProvider = new ActiveDataProvider([
    'query' => News::find()->orderBy('date DESC'),
    'pagination' => [
        'pageSize' => 20,
    ],
]);


?>
<h1>Список новостей</h1>
<table class="table-responsive">
    <tr>
        <td><a href="<?= Url::to(['create_news'])?>"> Создание новости</a> </td>
    </tr>
</table>
<p>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_listNews',
    ]) ?>
</p>
