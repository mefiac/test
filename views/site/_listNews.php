<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\Url;

?>

<div class="news-item">

    <h2><a href="<?= Url::to(['news/' . $model->translit_title]) ?>"><?= Html::encode($model->title) ?></h2>
    <?= HtmlPurifier::process($model->short_content) ?>
    <?= HtmlPurifier::process($model->date) ?>

    <p> <?= Html::a('Удалить статью', ['news/delete'], [
            'data' => [
                'method' => 'post',
                'params' => [
                    'id' => $model->id
                ],
            ],
        ]) ?>
    </p>
</div>