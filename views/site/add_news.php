<?php
/**
 * Created by PhpStorm.
 * User: kosty
 * Date: 18.08.2017
 * Time: 13:35
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n<div class=\"col-lg-5\">{input}</div>\n<div class=\"col-lg-3\">{error}</div>",
        'labelOptions' => ['class' => 'col-lg-6 control-label'],
    ],
]); ?>

<?= $form->field($model, 'title')->textInput(['autofocus' => true]) ?>
<?= $form->field($model, 'short_content')->textInput() ?>
<?= $form->field($model, 'content')->textarea() ?>
<?= $form->field($model, 'translit_title')->textInput()->label('Чпу ссылка, как в битриксе. Если оставить пустым, то автоматом сгенерит транслит'); ?>
<?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::classname(), [ 'dateFormat' => 'yyyy-MM-dd',
]) ?>

    <div class="form-group">
        <div class="col-lg-offset-10 col-lg-1">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>