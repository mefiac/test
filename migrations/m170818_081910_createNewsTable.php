<?php

use yii\db\Migration;

class m170818_081910_createNewsTable extends Migration
{
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'translit_title' => $this->string()->notNull(), // так будет получше чем, чемез urlmanager а индекс ускорит поиск
            'short_content' => $this->text(),
            'content' => $this->text(),
            'date' => $this->date(),
        ]);
        $this->createIndex(
            'idx-post-translit_title',
            'news',
            'translit_title'
        );
    }

    public function safeDown()
    {
        $this->dropTable('news');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170818_081910_createNewsTable cannot be reverted.\n";

        return false;
    }
    */
}
